#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

//void main()
//{
//	// Importing images
//	string path = "Resources/test.png";
//	Mat img = imread(path);
//	imshow("Image", img);
//	waitKey(0);
//
//}

void main()
{
	// Importing videos

	string path = "Resources/test_video.mp4";
	VideoCapture cap(path);
	Mat img;
	while (true)
	{
		cap.read(img);
		imshow("Image", img);
		waitKey(10);
	}
	


}