#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat img, imgGray, imgCanny, imgBlur, imgTh, imgDil, imgErode, imgWarp, imgCrop;
vector<Point> initial_pts, docPoints;
float w = 420, h = 596;

Mat preProcessing(Mat img)
{
	cvtColor(img, imgGray, COLOR_BGR2GRAY);

	// Adding a gaussian blur filter
	GaussianBlur(imgGray, imgBlur, Size(5, 5), 5, 0);

	// Adding canny edge detector
	Canny(imgBlur, imgCanny, 25, 75);

	// Imrpoving edge detection
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(imgCanny, imgDil, kernel);
	//erode(imgDil, imgErode, kernel);

	return imgDil;
}

vector<Point> getContours(Mat image) {


	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	findContours(image, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	//drawContours(img, contours, -1, Scalar(255, 0, 255), 2);
	vector<vector<Point>> conPoly(contours.size());
	vector<Rect> boundRect(contours.size());

	vector<Point> biggest;
	int maxArea = 0;

	for (int i = 0; i < contours.size(); i++)
	{
		int area = contourArea(contours[i]);
		cout << area << endl;

		string objectType;

		if (area > 1000)
		{
			float peri = arcLength(contours[i], true);
			approxPolyDP(contours[i], conPoly[i], 0.02 * peri, true);

			if (area > maxArea && conPoly[i].size() == 4)
			{
				maxArea = area;
				//drawContours(img, conPoly, i, Scalar(255, 0, 255), 5);
				biggest = { conPoly[i][0], conPoly[i][1], conPoly[i][2], conPoly[i][3] };
			}

			//drawContours(img, conPoly, i, Scalar(255, 0, 255), 2);
			//rectangle(img, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 5);
		}
	}
	return biggest;
}

void drawPoints(vector<Point> points, Scalar color)
{
	for (int i = 0; i < points.size();i++)
	{
		cout << points[i] << endl;
		//circle(img, points[i], 10, color, FILLED);
	}
}


vector<Point> reorder(vector<Point> points)
{
	vector<Point> newPoints;
	vector<int> sumPoints, subPoints;
	for (int i = 0; i < 4; i++)
	{
		sumPoints.push_back(points[i].x + points[i].y);
		subPoints.push_back(points[i].x - points[i].y);

	}
	newPoints.push_back(points[min_element(sumPoints.begin(), sumPoints.end())-sumPoints.begin()]);
	newPoints.push_back(points[min_element(subPoints.begin(), subPoints.end()) - subPoints.begin()]);
	newPoints.push_back(points[max_element(subPoints.begin(), subPoints.end()) - subPoints.begin()]);
	newPoints.push_back(points[max_element(sumPoints.begin(), sumPoints.end()) - sumPoints.begin()]);
	return newPoints;
}


Mat getWarp(Mat img, vector<Point> points, float w, float h)
{
	Mat matrix;
	Point2f src[4] = { points[0], points[2], points[1], points[3] };
	Point2f dest[4] = { {0.0f, 0.0f},{w, 0.0f},{0.0f, h},{w,h} };

	matrix = getPerspectiveTransform(src, dest);
	warpPerspective(img, imgWarp, matrix, Point(w, h));
	return imgWarp;
}

int main()
{
	string path = "Resources/paper.jpg";
	img = imread(path);
	
	//resize(img, img, Size(), 0.5, 0.5);

	// Preprocessing
	imgTh = preProcessing(img);
	imshow("ImageThreshold", imgTh);

	// Get contours
	initial_pts = getContours(imgTh);
	docPoints = reorder(initial_pts);
	drawPoints(docPoints, Scalar(0, 255, 0));
	imshow("Image", img);

	// Warp
	imgWarp = getWarp(img, docPoints, w, h);
	imshow("Image", imgWarp);

	// Crop
	Rect roi(5, 5, w - 10, h - 10);
	imgCrop = imgWarp(roi);
	imshow("ImageCrop", imgCrop);

	waitKey(0);
	return 0;
}