#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int getContours(Mat imgdil, Mat imgsrc)
{
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	findContours(imgdil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	float perimeter;
	vector<vector<Point>> conPoly(contours.size());
	vector<Rect> boundRect(contours.size());
	string objType;

	// Loop to filter out noise by computing area under contour
	for (int i = 0; i < contours.size(); i++)
	{
		float area = contourArea(contours[i]);
		
		if (area > 1000)
		{
			perimeter = arcLength(contours[i], true);
			approxPolyDP(contours[i], conPoly[i], 0.02*perimeter, true);

			cout<< conPoly[i].size()<<endl;
			boundRect[i] = boundingRect(conPoly[i]);
			rectangle(imgsrc, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 0, 255), 1);
			int objCorner = (int)conPoly[i].size();
			if (objCorner == 3)
			{
				objType = "Triangle";
			}
			if (objCorner == 4)
			{
				float aspRatio = float(boundRect[i].width) / float(boundRect[i].height);
				if (aspRatio > 0.95 && aspRatio< 1.05)
				{
					objType = "Square";
				}
				else
				{
					objType = "Rectangle";
				}				
			}
			if (objCorner > 4)
			{
				objType = "Circle";
			}
			// drawContours(imgsrc, contours, i, Scalar(0, 255, 0), 2);
			putText(imgsrc, objType, { boundRect[i].x, boundRect[i].y - 5 }, FONT_HERSHEY_PLAIN, 1.25, Scalar(0, 0, 0), 1);
		}
	}
	return 0;
}

void main()
{
	// Importing images
	string path = "Resources/shapes.png";
	Mat img = imread(path);

	// Preprocessing
	Mat imgGray, imgBlur, imgCanny, imgDil;
	cvtColor(img, imgGray, COLOR_BGR2GRAY);
	GaussianBlur(imgGray, imgBlur, Size(3, 3), 3, 0);
	Canny(imgBlur, imgCanny, 50, 100);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(imgCanny, imgDil, kernel);

	getContours(imgDil, img);

	imshow("Image", img);
	/*imshow("Image_Gray", imgGray);
	imshow("Blurred Image", imgBlur);
	imshow("Canny Image", imgDil);*/
	waitKey(0);

}
