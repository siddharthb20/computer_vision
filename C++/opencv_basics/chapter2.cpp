#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace std;
using namespace cv;

void main()
{
	string path = "Resources/test.jpg";
	Mat img = imread(path);
	Mat imgGray;
	Mat imgResize, imgBlur, imgCanny, imgDil, imgErode; 
	Mat imgCrop;

	resize(img, imgResize, Size(), 0.4, 0.4);

	cvtColor(imgResize, imgGray, COLOR_BGR2GRAY);

	// Adding a gaussian blur filter
	GaussianBlur(imgResize, imgBlur, Size(5, 5), 5, 0);

	// Adding canny edge detector
	Canny(imgBlur, imgCanny, 50, 100);

	// Imrpoving edge detection
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(imgCanny, imgDil, kernel);
	erode(imgDil, imgErode, kernel);

	Rect roi(200, 200, 500, 500);
	imgCrop = imgResize(roi);

	imshow("Image", imgResize);
	imshow("Image_Gray", imgGray);
	imshow("Blurred Image", imgBlur);
	imshow("Canny Image", imgDil);
	imshow("Erode Image", imgErode);
	imshow("Cropped Image", imgCrop);
	waitKey(0);
}