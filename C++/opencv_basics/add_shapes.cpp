#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

void main()
{
	Mat img(512, 512, CV_8UC3, Scalar(255, 255, 255));

	circle(img, Point(256, 256), 150, Scalar(0, 0, 255), FILLED);
	rectangle(img, Point(130, 226), Point(382, 286), Scalar(255, 255, 255), FILLED);
	line(img, Point(130, 296), Point(382, 296), Scalar(255, 255, 255), 1);
	putText(img, "Stop! No way forward", Point(106, 450), FONT_HERSHEY_COMPLEX_SMALL, 1.25, Scalar(0, 0, 0), 2);

	imshow("Image", img);
	waitKey(0);
}