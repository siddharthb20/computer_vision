import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, Flatten
from tensorflow.keras.layers import Dense, RepeatVector, Reshape, concatenate
from constants import IMG_SIZE, VGG_CLASSES
from tf.keras.layers import UpSampling2D, LeakyReLU
from tensorflow.keras import applications
from tensorflow.keras.models import Model

class Models():
    def __init__(self):
        self.inputs = None

    def generator(self):
        """
        Generator, built on top of convolutional part of the VGG16

        Returns
        -------
        gen_model : keras model
            Generator model.

        """
        input_img = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        pretrained_VGG = applications.vgg16.VGG16(weights='imagenet', \
            include_top=False, input_shape=(IMG_SIZE, IMG_SIZE, 3))
        VGG_model = Model(pretrained_VGG.input, pretrained_VGG.layers[-6].output)
        x = VGG_model(input_img)
        
        y = Conv2D(512, (3, 3), padding='same', strides=(2, 2), activation='relu')(x)
        y = BatchNormalization()(y)
        y = Conv2D(512, (3, 3), padding='same', strides=(1, 1), activation='relu')(y)
        y = BatchNormalization()(y)
        
        y = Conv2D(512, (3, 3), padding='same', strides=(2, 2), activation='relu')(y)
        y = BatchNormalization()(y)
        y = Conv2D(512, (3, 3), padding='same', strides=(1, 1), activation='relu')(y)
        y = BatchNormalization()(y)
        
        vgg = Flatten()(y)
        vgg = Dense(4096)(vgg)
        vgg = Dense(4096)(vgg)
        vgg = Dense(VGG_CLASSES, activation='softmax')(vgg)
        
        y = Flatten()(y)
        y = Dense(1024)(y)
        y = Dense(512)(y)
        y = Dense(256)(y)
        y = RepeatVector(784)(y)
        y = Reshape((28, 28, 256))(y)
        
        m = Conv2D(512, (3, 3), padding='same', strides=(1, 1), activation='relu')(x)
        m = BatchNormalization()(m)
        m = Conv2D(256, (3, 3), padding='same', strides=(1, 1), activation='relu')(m)
        m = BatchNormalization()(m)
        
        con_model = concatenate([m, y])
        
        con_output = Conv2D(256, (1, 1), padding='same', strides=(1, 1), activation='relu')(con_model)
        con_output = Conv2D(128, (3, 3), padding='same', strides=(1, 1), activation='relu')(con_output)

        con_output = UpSampling2D(size=(2, 2))(con_output)
        con_output = Conv2D(64, (3, 3), padding='same', strides=(1, 1), activation='relu')(con_output)
        con_output = Conv2D(64, (3, 3), padding='same', strides=(1, 1), activation='relu')(con_output)

        con_output = UpSampling2D(size=(2, 2))(con_output)
        con_output = Conv2D(32, (3, 3), padding='same', strides=(1, 1), activation='relu')(con_output)
        con_output = Conv2D(2, (3, 3), padding='same', strides=(1, 1), activation='sigmoid')(con_output)
        con_output = UpSampling2D(size=(2, 2))(con_output)
        
        gen_model = Model(input_img, [con_output, vgg])
        
        return gen_model
    
    def discriminator(self):
        
        img_l = Input(shape=(IMG_SIZE, IMG_SIZE, 1))
        img_ab = Input(shape=(IMG_SIZE, IMG_SIZE, 2))
        
        x = concatenate([img_l, img_ab])
        x = Conv2D(64, (4, 4), padding='same', strides=(2, 2))(x)
        x = LeakyReLU()(x)
        x = Conv2D(128, (4, 4), padding='same', strides=(2, 2))(x)
        x = LeakyReLU()(x)
        x = Conv2D(256, (4, 4), padding='same', strides=(2, 2))(x)
        x = LeakyReLU()(x)
        x = Conv2D(512, (4, 4), padding='same', strides=(1, 1))(x)
        x = LeakyReLU()(x)
        x = Conv2D(1, (4, 4), padding='same', strides=(1, 1))(x)
        x = LeakyReLU()(x)

        disc_model = Model([img_ab, img_l], x)
        return disc_model
        
        