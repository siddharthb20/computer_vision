from tensorflow.keras.optimizers import Adam
import psutil
import os

IMAGE_FORMATS = ['.jpg', '.jpeg', '.png']
IMG_SIZE = 224
BATCH_SIZE = 16
OPT = Adam(0.00002, 0.5)
GRADIENT_PENALTY_WEIGHT = 10
EPOCHS = 5
VGG_CLASSES = 1000

def process_memory():
    """
    Function to measure memory consumption, maybe needed during training

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss

def profile(func):
    def wrapper(*args, **kwargs):
        mem_before = process_memory()
        result = func(*args, **kwargs)
        mem_after = process_memory()
        print(f'Consumed memory: {mem_after-mem_before}')
        return result
    return wrapper