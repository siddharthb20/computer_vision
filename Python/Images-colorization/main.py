import argparse
import os
import tensorflow as tf
from constants import IMAGE_FORMATS
from train import Trainer

def parse_args():

    parser = argparse.ArgumentParser(description="Program for coloring B/W images")

    # Paths
    parser.add_argument('-i', '--input_image', help='Path to input image', required=False)
    parser.add_argument('-d', '--input_dir', \
        help='Path to input image directory. Loads all images in directory', required=False)


    # Boolean
    parser.add_argument('-g', '--gpu_available', help='Is GPU available?', action='store_true')

    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    if not args.gpu_available:
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        
    # Check if GPU available, if yes, use GPU
    if tf.test.gpu_device_name():
        print('Using GPU')
    else:
        print("No GPU found")
        
    # Check if input is proper image        
    if args.input_image is not None:
        folder_path, file_name = os.path.split(args.input_image)
        file_name, ext = os.path.splitext(file_name)
        if ext not in IMAGE_FORMATS:
            raise TypeError('Invalid input file format')

    trainer = Trainer(args.input_dir)
    
    # Train model
    trainer.train()

if __name__ == '__main__':
    main()