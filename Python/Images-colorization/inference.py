# -*- coding: utf-8 -*-
"""
Created on Sat Mov 26 18:44:04 2022

@author: Siddharth
"""

import argparse
import os
# import streamlit
import numpy as np
import cv2
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.python.ops.numpy_ops import np_config
np_config.enable_numpy_behavior()


def parse_args():
    parser = argparse.ArgumentParser(description='App for converting BW images to color')

    # Paths
    parser.add_argument('-i', '--input_image', help='Path to input image', required=False)
    parser.add_argument('-d', '--input_dir', \
        help='Path to input image directory. Loads all images in directory', required=False)
    parser.add_argument('-m', '--model_path', help='Path to trained model', required=True)

    # Boolean
    parser.add_argument('-g', '--gpu_available', help='Is GPU available?', action='store_true')
    parser.add_argument('-s', '--show_gray', help='Show gray image?', action='store_true')

    args = parser.parse_args()
    return args

def decompress_image(image):
    """
    Function to decompress pixel values from [0, 1] to [0, 255]

    Parameters
    ----------
    image : numpy array
        Output image from the neural network. Values in range [0, 1]

    Returns
    -------
    image : numpy array
        Image stored as int8 array. Values in range [0, 255]

    """
    if not (type(image).__module__ == np.__name__):
        image = image.numpy()

    image = image*255
    image[image < 0] = 0
    image[image > 255] = 255
    return image.astype(np.uint8)


def reconstruct(original, predicted):
    """
    Reconstruct RGB image from Lab image

    Parameters
    ----------
    original : numpy array
        Original grayscale image of shape (HxWx1). L component of Lab
    predicted : numpy array
        ab component of Lab. Image shape (HxWx2)

    Returns
    -------
    img_recons : numpy array
        Reconstructed image from L and ab components and converted to RGB.

    """
    img_recons = np.concatenate((original, predicted), axis=2)
    img_recons = cv2.cvtColor(img_recons, cv2.COLOR_Lab2BGR)
    return img_recons

def pre_process_image(image):
    """
    Function to pre process image before feeding to the GAN.

    Parameters
    ----------
    image : numpy array
        Input jpeg or png image.

    Returns
    -------
    resized_image : numpy array
        Input grayscale image for the neural network. Shape (224, 224, 1).
    img_dims : list
        List containing the shape of the input image.
    im : numpy array
        Original grayscale image to be reconstructed with ab components later.

    """
    img_shape = image.shape
    IMAGE_SHAPE = 224
    if img_shape[2] > 1:
        resized_img = cv2.resize(image, (IMAGE_SHAPE, IMAGE_SHAPE))
        img = cv2.cvtColor(resized_img, cv2.COLOR_BGR2Lab)
        resized_img = img/255
    img_dims = [img_shape[0], img_shape[1]]
    im = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)
    return np.reshape(resized_img[:, :, 0], (IMAGE_SHAPE, IMAGE_SHAPE, 1)), img_dims, \
        np.reshape(im[:, :, 0], (img_dims[0], img_dims[1], 1))/255

def main():
    args = parse_args()

    if not args.gpu_available:
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        
    # Check if GPU available, if yes, use GPU
    if tf.test.gpu_device_name():
        print('Using GPU')
    else:
        print("No GPU found")


    model = load_model(args.model_path)

    img = cv2.imread(args.input_image)
    
    input_img, img_dims, lab_img_original = pre_process_image(img)

    if args.show_gray:
        cv2.imshow('image', lab_img_original)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    height, width = img_dims[0], img_dims[1]
    img_in = np.tile(input_img,[1, 1, 1, 3])
    
    preds, _ = model.predict(np.tile(input_img, [1, 1, 1, 3]))
    
    # Reverse the preprocessing modifications on ab image outputs
    y_pred = decompress_image(preds)
    y_pred = cv2.resize(y_pred[0], (width, height))
    lab_img_original = decompress_image(lab_img_original)

    # Combine ab outputs with original 'L-part' and convert to BGR
    final_image = reconstruct(lab_img_original, y_pred)

    output_path = os.path.join(os.getcwd(), 'output.jpg')
    # cv2.imwrite(output_path, final_image)
    cv2.imshow('image', final_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
