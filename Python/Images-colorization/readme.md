# Image colorization

The idea behind this project is to add color to black and white images. The model used for this was the ChromaGAN model by Patricia Vitoria, Lara Raad and Coloma Ballester. You can read more about their work [here](https://arxiv.org/abs/1907.09837). <br><br>

In this model, the authors have essentially split the generator into two parts: global features and the midlevel features. These are combined with the classes labels and then their fusion is the generator output. There is a PatchGAN discriminator to aid the generator during training. The deep learning framework I used was Keras for my own implementation. <br><br>

To try this model for yourself, clone the repository. It is advisable to try this in a new environment. The documentation on creating a new environment is available [here](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiphb-T4J_-AhUBr6QKHWMAB1sQFnoECBAQAQ&url=https%3A%2F%2Fdocs.conda.io%2Fprojects%2Fconda%2Fen%2F4.6.0%2F_downloads%2F52a95608c49671267e40c689e0bc00ca%2Fconda-cheatsheet.pdf&usg=AOvVaw3uUYEqas7NMuAmCCWAx_yl). Then the required packages can be installed using the `requirements.txt` file. <br><br>

`pip install -r requirements.txt`<br><br>

Before running inference, you may need to download the model, if not already downloaded. The model can be downloaded by either using the following command or visiting the link below:<br><br>
`wget http://dev.ipol.im/~lraad/chromaGAN/model/my_model_colorization.h5`<br><br>

To run **inference.py** execute the following command: <br><br>

`python inference.py -i <grayscale image location> -m <model location>`<br><br>

If GPU is available, add `-g` flag. <br>
To view the original B/W image, add `-s` flag.<br>

To train the model, download the training dataset and extract the images. Then run the following the command:<br>
`python main.py -d <path to images>`<br><br>
If GPU is available, add `-g` flag. <br>