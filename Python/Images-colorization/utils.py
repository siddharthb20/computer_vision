import tensorflow as tf
from keras import backend as K
import numpy as np
import googleapiclient.discovery
from google.api_core.client_options import ClientOptions
import json
import cv2
from google.cloud import aiplatform

def wasserstein_loss(y_true, y_pred):
    """
    Helper function to compute the wasserstein loss.

    Parameters
    ----------
    y_true : Keras tensor
        Actual value tensor.
    y_pred : Keras tensor
        Predicted value tensor.

    Returns
    -------
    Keras tensor
        Wasserstein loss.

    """
    return tf.reduce_mean(y_pred)


def gradient_penalty_loss(y_true, y_pred, averaged_samples, gradient_penalty_weight):
    """
    

    Parameters
    ----------
    y_true : Keras tensor
        True labels. Shape: (16, 28, 28, 1)
    y_pred : Keras tensor
        Predicted labels. Shape: (16, 28, 28, 1)
    averaged_samples : Keras tensor
        Averaged samples from predicted and actual labels for discriminator.
        Shape: (16, 224, 224, 2)
    gradient_penalty_weight : int
        Penalty factor on the change of weights to regularize delta.

    Returns
    -------
    gradient_penalty : Keras tensor
        Computed penalty on the gradients.

    """
    gradients = K.gradients(y_pred, averaged_samples)[0]
    gradients_sqr = K.square(gradients)
    gradients_sqr_sum = K.sum(gradients_sqr, \
                              axis=np.arange(1, len(gradients_sqr.shape)))
    gradient_l2_norm = K.sqrt(gradients_sqr_sum)
    gradient_penalty = gradient_penalty_weight*K.square(1 - gradient_l2_norm)
    return K.mean(gradient_penalty)

def load_image(filename):
    """
    Helper function to load the image files.

    Parameters
    ----------
    filename : str
        Absolute path of the image file.

    Returns
    -------
    img : Keras tensor
        Image tensor to be used with the neural network.
    original_gray : numpy array
        Grayscale image with the original image resolution.
    gray_img : numpy array
        Gray image generated with OpenCV.

    """
    
    img = tf.io.decode_image(filename, channels=3)
    img_array = img.numpy()
    gray_img = cv2.cvtColor(img_array, cv2.COLOR_BGR2GRAY)
    original_gray = cv2.cvtColor(img_array, cv2.COLOR_BGR2Lab)
    h, w, _ = original_gray.shape
    img_array = cv2.resize(img_array, (200, 200))
    height, width, channels = img_array.shape
    img_array = cv2.cvtColor(img_array, cv2.COLOR_BGR2Lab)
    img = img_array[:, :, 0].reshape((height, width, 1))
    img = np.tile(img, [1, 1, 1, 3])
    # img = tf.expand_dims(img, axis=0)
    img = tf.cast(img, tf.float16)
    
    return img, original_gray[:, :, 0].reshape((h, w, 1)), gray_img

def colorization(project, region, model, instance, version=None):
    """
    Helper function to ping the cloud model to obtain predictions. 
    Converts input tensor to lists and signatures and packages them as a json
    file. Conversion of tensor/numpy arrays to list is required because tensors
    or arrays are not json compatible. Original unmodified script can be found
    in official Google Cloud documentation.

    Parameters
    ----------
    project : str
        Name of the gcloud project.
    region : str
        Name of the region where cloud resources have been blocked.
    model : str
        Name of the cloud copy of the model.
    instance : Tensor
        Input tensor to the model.
    version : str, optional
        Version of the model to be used if multiple options available. 
        The default is None.

    Raises
    ------
    RuntimeError
        Throw and error if the predicted values are None.

    Returns
    -------
    output_ab_image : numpy array
        ab component of predicted Lab images. Shape: (16, 224, 224, 2)

    """
    prefix = "{}-ml".format(region) if region else "ml"
    api_endpoint = "https://{}.googleapis.com".format(prefix)
    client_options = ClientOptions(api_endpoint=api_endpoint)

    model_path = 'projects/{}/models/{}'.format(project, model)
    if version is not None:
        model_path += '/versions/{}'.format(version)
    
    aiplatform.init(project='#######', staging_bucket='##########') # change
    
    ml_resource = googleapiclient.discovery.build(
        "ml", "v1", cache_discovery=False, client_options=client_options).projects()
    
    instance_list = instance.numpy().tolist()

    input_data_json = {"signature_name": "serving_default",
                        "instances": instance_list}
    with open('data.json', 'w', encoding='utf-8') as f:
        json.dump(input_data_json, f, ensure_ascii=False, indent=4)

    endpoint = aiplatform.Endpoint(endpoint_name="####") # change to endpoint
    print(f'Waiting for model result')
    result = endpoint.predict(instances=instance_list).predictions

    # my_model=aiplatform.Model("projects/706786288011/locations/us-central1/models/5955484940639404032") 
    # request = ml_resource.predict(name=model_path, body=input_data_json)
    # response = request.execute()

    # response = my_model.batch_predict(
    # job_display_name='image_colorization_prediction',
    # gcs_source='gs://image_colorization_20122022/data.json',
    # gcs_destination_prefix='gs://image_colorization_20122022/prediction-results',
    # machine_type='n1-standard-2',)

    if "error" in result:
        raise RuntimeError(result['error'])
    
    for k, v in result[0].items():
        
        if len(v)<1000:
            response = v
    
    response = np.array(response)

    output_ab_image = response

    return output_ab_image

def decompress_image(image):
    """
    Function to decompress pixel values from [0, 1] to [0, 255]

    Parameters
    ----------
    image : numpy array
        Output image from the neural network. Values in range [0, 1]

    Returns
    -------
    image : numpy array
        Image stored as int8 array. Values in range [0, 255]

    """
    if not (type(image).__module__ == np.__name__):
        image = image.numpy()

    image = image*255
    image[image < 0] = 0
    image[image > 255] = 255
    return image.astype(np.uint8)

def reconstruct(original, predicted):
    """
    Reconstruct RGB image from Lab image

    Parameters
    ----------
    original : numpy array
        Original grayscale image of shape (HxWx1). L component of Lab
    predicted : numpy array
        ab component of Lab. Image shape (HxWx2)

    Returns
    -------
    img_recons : numpy array
        Reconstructed image from L and ab components and converted to RGB.

    """
    img_recons = np.concatenate((original, predicted), axis=2)
    img_recons = cv2.cvtColor(img_recons, cv2.COLOR_Lab2RGB)
    return img_recons