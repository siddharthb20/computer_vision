import cv2
import os
import numpy as np
import gc
from constants import IMG_SIZE, BATCH_SIZE, IMAGE_FORMATS, OPT, \
    GRADIENT_PENALTY_WEIGHT, EPOCHS
from model import Models
from utils import wasserstein_loss, gradient_penalty_loss
from tensorflow.keras.layers import Input
from keras import backend as K
from functools import partial
from tensorflow.keras.models import Model
from tensorflow.keras import applications
import tensorflow as tf
from tensorflow.python.framework.ops import disable_eager_execution
from tqdm import tqdm 
import tracemalloc
from datetime import datetime
disable_eager_execution()

class Trainer():
    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        self.file_names = self.get_files(data_dir)
        self.num_files = len(self.file_names)
        self.models = Models()
        self.save_dir = os.getcwd()
        
    def load_image(self, imagePath):
        """
        Method to read image and return grayscale and color components
        separately

        Parameters
        ----------
        imagePath : str
            Location of the input image.

        Returns
        -------
        img_lab : numpy array
            L component of Lab image resized for NN. Shape (SIZExSIZEx1)
        img_lab : numpy array
            ab component of the original Lab image. Shape (HxWx2)
        image : numpy array
            Original RGB image.
        img_lab_original : numpy array
            Original grayscale image. Shape (HxWx1)
        size : int
            File size in bytes.

        """
        image = cv2.imread(imagePath)
        size = os.path.getsize(imagePath)
        # print(f'Image size: {size//1024} kB')
        img_lab_original = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)        
        img_lab = cv2.resize(img_lab_original, (IMG_SIZE, IMG_SIZE))        
        return np.reshape(img_lab[:, :, 0], (IMG_SIZE, IMG_SIZE, 1)), \
            img_lab[:, :, 1:], image, img_lab_original[:, :, 0], size
    
    
    def augmentation(self, batch, augment=False):
        """
        Method for performing image augmentation

        Parameters
        ----------
        batch : int
            Batch number being processed.
        augment : bool, optional
            Perform image augmentation. The default is False.

        Returns
        -------
        img_l : numpy array
            L component of input image.
        img_ab : numpy array
            ab component of input image.
        TYPE
            DESCRIPTION.
        lab_original : TYPE
            DESCRIPTION.
        batch_file_names : TYPE
            DESCRIPTION.

        """
        if not augment:
            # print('Skipping image augmentation')
            start_index = batch*BATCH_SIZE
            img_l, img_ab, original, lab_original = [], [], [], []
            # for i in range(num_batches):
            #     index = np.random.choice(all_index, size=BATCH_SIZE, \
            #                              replace=False)
            #     batch_file_names = [os.path.join(self.data_dir, f) \
            #                         for f in [self.file_names[g] for g in index]]
            index = list(range(start_index, start_index+BATCH_SIZE))
            batch_file_names = [os.path.join(self.data_dir, f) \
                                for f in [self.file_names[g] for g in index]]
            batch_size = 0
            for b in batch_file_names:
                l, ab, org, lab_org, size = self.load_image(b)
                batch_size += size
                img_l.append(l)
                img_ab.append(ab)
                # original.append(org)
                # lab_original.append(lab_org)
            img_l = np.asarray(img_l)/255
            img_ab = np.asarray(img_ab)/255
            # original = np.asarray(original)
            # lab_original = np.asarray(lab_original)/255
            print(f'Batch size = {batch_size/1024} kB')
            return img_l, img_ab, # original, lab_original, batch_file_names
            
    
    
    def get_files(self, data_dir):
        """
        Method to recursively get list of all image files.

        Parameters
        ----------
        data_dir : str
            Location of the image data.

        Returns
        -------
        images : list
            List of absolute paths of all images.

        """
        sub_folders = [f.path for f in os.scandir(data_dir) if f.is_dir()]
        images_subfolders = [[os.path.join(f, g) for g in os.listdir(f) if \
                   (os.path.splitext(g)[1]).lower() in IMAGE_FORMATS] \
                  for f in sub_folders]
            
        image_paths = [os.path.join(data_dir, f) for f in \
                       sorted(os.listdir(data_dir)) if \
                       (os.path.splitext(f)[1]).lower() in IMAGE_FORMATS]
        
        images = [i for j in images_subfolders for i in j]
        images.extend(image_paths)
        return images
    
    
    def merge(self, inputs):
        weights = K.random_uniform((BATCH_SIZE, 1, 1, 1))
        return (weights*inputs[0]) + ((1-weights)*(inputs[1]))
        
    
    def train(self):
        """
        Method to initialize and run the gen and disc NNs.

        Returns
        -------
        None.

        """
        
        print('Initializing models')
        
        # Setup generator
        self.generator = self.models.generator()
        self.generator.compile(loss=['mse', 'kld'], optimizer=OPT)
        print('Generator compiled')
        self.generator.trainable = False
        
        # Setup discriminator
        self.discriminator = self.models.discriminator()
        self.discriminator.compile(loss=wasserstein_loss, optimizer=OPT)
        print('Discriminator compiled')
        
        # Setup input tensors
        img_l_3 = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        img_l_1 = Input(shape=(IMG_SIZE, IMG_SIZE, 1))
        img_ab_real= Input(shape=(IMG_SIZE, IMG_SIZE, 2))
        
        # Get ab predictions and class predictions from the generator
        img_ab_pred, class_pred = self.generator(img_l_3)
        
        # Get discriminator outputs for actual and predicted ab images
        disc_pred = self.discriminator([img_ab_pred, img_l_1])
        disc_real = self.discriminator([img_ab_real, img_l_1])
        
        # Merge real and predicted outputs to train the discriminator
        sample_mean = self.merge([img_ab_real, img_ab_pred])
        disc_avg_samples = self.discriminator([sample_mean, img_l_1])
        partial_loss = partial(gradient_penalty_loss, \
                               averaged_samples=sample_mean, \
                               gradient_penalty_weight=GRADIENT_PENALTY_WEIGHT)
        partial_loss.__name__ = 'gradient_penalty'
        
        self.discriminator_model = Model([img_l_1, img_ab_real, img_l_3], \
                                         [disc_real, disc_pred, \
                                          disc_avg_samples])
        self.discriminator_model.compile(optimizer=OPT, loss=[wasserstein_loss, \
                                                              wasserstein_loss, \
                                                              partial_loss], \
                                         loss_weights=[-1.0, 1.0, 1.0])
            
        self.generator.trainable = True
        self.discriminator.trainable = False
        self.combined = Model([img_l_3, img_l_1], [img_ab_pred, class_pred, \
                                                   disc_pred])
        
        self.combined.compile(loss=['mse', 'kld', wasserstein_loss],
                           loss_weights=[1.0, 0.003, -0.1],
                           optimizer=OPT)
        
        
        # All models compiled, commence training
        print('Starting training')
        VGG_pretrained = applications.vgg16.VGG16(weights='imagenet', \
                                                  include_top=True)
        
        # 1 for real images, -1 for synthetic images
        y_pos = np.ones((BATCH_SIZE, 1), dtype=np.float32)
        y_neg = -y_pos
        y_neu = np.ones((BATCH_SIZE, 1), dtype=np.float32)
        
        # data_gen = self.augmentation()

        tracemalloc.start()

        f = open("logs.txt", "w+")
        
        for epoch in range(EPOCHS):
            for b in tqdm(range(self.num_files//BATCH_SIZE)):
                snap1 = tracemalloc.take_snapshot()
                train_l, train_ab = self.augmentation(b)
                
                img_l_3 = np.tile(train_l, [1, 1, 1, 3])
                
                img_l_3 = tf.cast(img_l_3, tf.float32)
            
                VGG_output = VGG_pretrained(img_l_3)
                
                train_l = tf.cast(train_l, tf.float32)
                train_ab, y_pos = tf.cast(train_ab, tf.float32), tf.cast(y_pos, tf.float32)
                VGG_output = tf.cast(VGG_output, tf.float32)
                
                y_neg, y_neu = tf.cast(y_neg, tf.float32), tf.cast(y_neu, tf.float32)
                
                gen_loss = self.combined.train_on_batch([img_l_3, train_l],
                                                        [train_ab, VGG_output, y_pos])

                disc_loss = self.discriminator_model.train_on_batch([train_l, train_ab, img_l_3],
                                                         [y_pos, y_neg, y_neu])
                if (b)%200 == 0:
                    print(f'Epoch: {epoch+1}, Batch: {b+1}, G loss: {gen_loss[0]}, D loss: {disc_loss[0]}')
                snap2 = tracemalloc.take_snapshot()
                stats = snap2.compare_to(snap1, 'lineno')
                for stat in stats:
                    timestamp = datetime.now().replace(microsecond=0)
                    log = str(timestamp) + '\t' + str(stat) + '\n'
                    f.write(log)

                # tf.keras.backend.clear_session()
                _ = gc.collect()

            combined_model_path = os.path.join(self.save_dir, 'combined.h5')
            self.combined.save(combined_model_path)
            
            gen_model_path = os.path.join(self.save_dir, 'generator.h5')
            self.generator.save(gen_model_path)
            
            disc_model_path = os.path.join(self.save_dir, 'discriminator.h5')
            self.discriminator.save(disc_model_path)
