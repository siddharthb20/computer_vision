# Image Super-resolution

This goal of this project is to double the resolution of images/videos. The model used to accomplish this is based on Enhanced Deep Super-resolution Network (EDSR) by Bee Lim and team. The deep learning framework used is Tensorflow. I have also add a coarsely trained model for direct inference without actually having to train a model for scratch. The dataset used for training is the Div2K dataset. However, I have not included it here due to it's large size.

This folder contains all the python scripts needed for both training and inference. Before running any files, you may need to install the dependencies. For this, the following command needs to be run from your terminal: <br><br>
`pip install -r requirements.txt`<br><br>
To run **inference** you can clone the repo and use the `edsr_model.h5` model for inference. `cd` into the folder where the files are stored and execute the following command: <br><br>
`python main.py -i <link to image> -m <link to model> -t` <br><br>
`-g` flag can also be added if a GPU is available. I have tested these commands on Windows. For videos, change the `-i` flag to `-v` and add the link to the video. By default, the output is stored in the folder containing all the scripts. But this can be changed with the `-o` flag. <br><br><br>

For training, the Div2K dataset will have to be downloaded and placed in the folder with the scripts. This dataset is available on Kaggle. In future I plan to expand it to accommodate more datasets. However, for now, Div2K dataset is required. To train, execute the following command <br><br>
`python main.py` <br><br>
You can add other flags depending your requirement: <br>
`-g` to enable GPU usage, if available. <br>
`-e` for the number of training epochs. Default value is 200. <br>
`-q` for performing quantization. Large models can be difficult to upload. Default value is 0. <br>
`-r` for the number of ResNet blocks. Default value is 32. <br>
`-s` for the scaling factor. Default value is 2. The model is currently configured for doubling. In future, I plan to expand to other values. <br>
`-b` for the number of batches. Default value is 16. <br>
`-lr` for the learning rate. Default value is 0.0001. <br>