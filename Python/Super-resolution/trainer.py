# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 20:56:16 2022

@author: Siddharth
"""

import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import albumentations as A
from tqdm import tqdm
from EDSR import EDSR
from keras.metrics import Mean
from keras.losses import MeanAbsoluteError
# from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers.schedules import PiecewiseConstantDecay
# import tensorflow_dataset as tfds
from dataloader import DataLoader
from tensorflow.keras.models import load_model


class Trainer():
    
    def __init__(self,
                 data_dir,
                 scale,
                 rgb_mean,
                 epochs,
                 save_loc):
        
        self.data_dir = os.path.join(data_dir, 'Div2K')
        self.scale = scale
        self.rgb_mean = rgb_mean
        self.loss = MeanAbsoluteError()
        self.lr = PiecewiseConstantDecay(boundaries=[20000], values=[1e-4, 5e-5])
        self.metric = Mean()
        self.epochs = epochs
        self.save_loc = save_loc
        self.checkpoint = None
        
    
    def restore(self):
        """
        Method for restoring the last checkpoint in training.

        Returns
        -------
        None.

        """
        if self.checkpoint_manager.latest_checkpoint:
            self.checkpoint.restore(self.checkpoint_manager.latest_checkpoint)
            print('Model loaded', self.checkpoint.step.numpy())
    
    
    def load_data(self, download=True):
        """
        Method to create training and validation datasets for low and high 
        resolution images.

        Parameters
        ----------
        download : boolean, optional
            Download data from ETH server or use local data? 
            The default is True.

        Returns
        -------
        train_dataset : Tensorflow dataset
            Tensorflow dataset containing patches of training images.
            
        valid_dataset : Tensorflow dataset
            Tensorflow dataset containing validation images.

        """
        if not download:
            train_HR_dir = os.path.join(self.data_dir, 'DIV2K_train_HR')[:100]
            train_LR_dir = os.path.join(self.data_dir, 'DIV2K_train_LR')[:100]
            valid_HR_dir = os.path.join(self.data_dir, 'DIV2K_valid_HR')
            valid_LR_dir = os.path.join(self.data_dir, 'DIV2K_valid_LR')
            train_HR = os.listdir(train_HR_dir)
            train_LR = os.listdir(train_LR_dir)
            valid_HR = os.listdir(valid_HR_dir)
            valid_LR = os.listdir(valid_LR_dir)
            
            print("Loading training images")
            train_img_HR = [cv2.imread(os.path.join(train_HR_dir, img))
                                       for img in tqdm(train_HR)]
            train_img_LR = [cv2.imread(os.path.join(train_LR_dir , img)) 
                                       for img in train_LR]
            print("Loading validation images")
            valid_img_HR = [cv2.imread(os.path.join(valid_HR_dir, img)) 
                                       for img in tqdm(valid_HR)]
            valid_img_LR = [cv2.imread(os.path.join(valid_LR_dir , img)) 
                                       for img in valid_LR]
            
            return train_img_HR, train_img_LR, valid_img_HR, valid_img_LR
    
        if download:
            
            train_data_obj = DataLoader()
            valid_data_obj = DataLoader(subset='valid')
            train_dataset = train_data_obj.compile_dataset(batch_size=16)
            valid_dataset = valid_data_obj.compile_dataset(batch_size=1, 
                                                           random_transform=False, 
                                                           repeat_count=1)
            
            return train_dataset, valid_dataset
            
    
    
    def visualize_data(self, dataset, num_images):
        """
        Method to plot images. Plots sets of corresponding low and high 
        resolution images.

        Parameters
        ----------
        dataset : Tensorflow dataset object
            Dataset of images (could be either training or validation data).
        num_images : int
            Number of images to be plotted.

        Returns
        -------
        None.

        """
        plt.subplots(nrows=num_images, ncols=2, figsize=(25,25))
        i = 1
        for LR, HR in dataset.take(num_images).as_numpy_iterator():
            print('LR Shape is {} and HR shape is {}'.format(LR[0].shape, HR[0].shape))
            plt.subplot(num_images, 2, i), plt.imshow(LR[0]), plt.axis('off')
            plt.subplot(num_images, 2, i+1), plt.imshow(HR[0]), plt.axis('off')
            i = i+2
        plt.show()
        
        
    def augmenter(self, img_HR, img_LR, aug_factor=2, 
                  batch_size=32, randomize=True, train=True):
        
        print("Performing augmentation...")
        print(len(img_HR), len(img_LR))
        # transform = A.Compose([
        #     A.ShiftScaleRotate(shift_limit=0.625, rotate_limit=90, p=0.85)],
        #     additional_targets={'image_LR':'image'})
        
        all_indices = np.arange(len(img_HR))
        # HR_images, LR_images = [], []
        batch_size = batch_size//aug_factor
        left_indices = set(all_indices)
        print(len(left_indices), batch_size)
        # while (len(left_indices) > batch_size):
            
        # if randomize:
        #     train_data_idx = np.random.choice(list(left_indices), 
        #                                       size=batch_size, 
        #                                       replace=False)
        #     left_indices = set(left_indices)-set(train_data_idx)
            
        # else:
        #     train_data_idx = all_indices[:batch_size]
        #     all_indices = all_indices[batch_size:]
        
        
        for i in range(len(img_HR)):
            print(img_LR[i].shape)
            yield tf.convert_to_tensor(img_LR[i]), tf.convert_to_tensor(img_HR[i])
        '''
        print(len(HR_images), len(LR_images))

        
        for i in range(aug_factor-1):
            for j in range(len(HR_images)):
                transformed = transform(image=img_HR[j], image_LR=img_LR[j])
                HR_images.append(np.array(transformed['image']).astype(np.float32))
                LR_images.append(np.array(transformed['image_LR']).astype(np.float32))
            
        print(f'End of loop: {len(HR_images)}')'''
        # yield LR_images, HR_images
            
            
        
    def create_model(self, show_model=False):
        """
        Method for creating blank model with random weights. Can be used for 
        training or loading weights and then performing inference.

        Parameters
        ----------
        show_model : boolean, optional
            Print the model architecture. The default is False.

        Returns
        -------
        model : Tensorflow model
            Keras model according to EDSR paper.

        """
        model_obj = EDSR(self.rgb_mean, self.scale)
        RES_BLOCKS = 16
        model = model_obj.super_scaler(res_blocks=RES_BLOCKS)
        if show_model:
            model.summary()
        return model
    
    
    def train_model(self, download=True):
        """
        Method for starting/continuing training from last saved checkpoint.

        Parameters
        ----------
        download : boolean, optional
            Flag to decide if local data must be used or downloaded. 
            The default is True.

        Returns
        -------
        history : list
            Loss data recorded during training for plotting later.

        """
        model = self.create_model(show_model=False)
        model.compile(optimizer='adam', 
                      loss=self.loss, metrics=self.metric)
        self.checkpoint = tf.train.Checkpoint(step=tf.Variable(0),
                                              psnr=tf.Variable(-1.0),
                                              optimizer=tf.keras.optimizers.Adam(learning_rate=self.lr, epsilon=1e-08),
                                              model=model)
        self.checkpoint_manager = tf.train.CheckpointManager(checkpoint=self.checkpoint,
                                                             directory=self.save_loc,
                                                             max_to_keep=3)
        
        self.restore()
        
        if download:
            train_data, valid_data = self.load_data()
            print("Plotting images")
            self.visualize_data(train_data, 2)
            steps = 300000
            checkpoint = self.checkpoint
            evaluate_every = 1000
            save_best = False
            loss_mean = tf.keras.metrics.Mean()
            checkpoint_manager = self.checkpoint_manager
            
            for LR, HR in tqdm(train_data.take(steps - checkpoint.step.numpy())):
                
                checkpoint.step.assign_add(1)
                step = checkpoint.step.numpy()
                loss = self.training_step(LR, HR)
                loss_mean(loss)
                
                # Save a checkpoint every 1000 steps
                if step % evaluate_every == 0:
                    loss_value = loss_mean.result()
                    loss_mean.reset_states()
                    psnr = self.get_psnr(valid_data)
                    if step % 50 == 0:
                        print(f'{step+1}/{steps} | LOSS={loss_value.numpy():.4f} | PSNR = {psnr.numpy():3f}')
                    
                    if save_best and psnr <=checkpoint.psnr:
                        continue
                    
                    checkpoint.psnr = psnr
                    checkpoint_manager.save()
                    
            self.save_weights(model)
        else:
            train_HR, train_LR, valid_HR, valid_LR = self.load_data(download=False)
            augmentation_factor = 2
            batch_size = 32
            train_gen = self.augmenter(train_HR, train_LR, 
                                       aug_factor=augmentation_factor, batch_size=32)
            valid_gen = self.augmenter(valid_HR, train_LR, 
                                       aug_factor=augmentation_factor, batch_size=32)
            
            history = model.fit(train_gen,
                                epochs=self.epochs,
                                validation_data=valid_gen)
            if self.save_loc is not None:
                model_name = os.path.join(self.save_loc, 'super_resolution.h5')
                model.save(model_name)
                
            return history
        
    def training_step(self, LR, HR):
        """
        Method to perform a forward pass through the network.

        Parameters
        ----------
        LR : numpy array
            Low resolution patch.
        HR : numpy array
            Corresponding high resolution patch.

        Returns
        -------
        get_loss : Loss object
            Loss computed after current forward pass.

        """
        with tf.GradientTape() as tape:
            LR = tf.cast(LR, tf.float32)
            HR = tf.cast(HR, tf.float32)
            
            output = self.checkpoint.model(LR, training=True)
            get_loss = self.loss(HR, output)
        
        gradients = tape.gradient(get_loss, self.checkpoint.model.trainable_variables)
        self.checkpoint.optimizer.apply_gradients(zip(gradients, self.checkpoint.model.trainable_variables))

        return get_loss
        
    def get_psnr(self, dataset):
         """
         Method for computing the Peak SNR value. Save model for better PSNR 
         values.

         Parameters
         ----------
         dataset : Tensorflow dataset
             Input data for performing forward pass during training.

         Returns
         -------
         psnr : Tensorflow float
             PSNR value for evaluating the performance of current model.

         """
         psnr = []
         for LR, HR in dataset:
             LR = tf.cast(LR, tf.float32)       
             output = self.checkpoint.model(LR)
             output = tf.clip_by_value(output, 0, 255)
             output = tf.round(output)
             output = tf.cast(output, tf.uint8)
             psnr_value = tf.image.psnr(HR, output, max_val=255)[0]
             psnr.append(psnr_value)
         return tf.reduce_mean(psnr)
    
    def save_weights(self, model):
        model.save_weights('edsr_model.h5')
        
    def inference_image(self, model, LR):
        """
        Method to perform inference on low resolution image.

        Parameters
        ----------
        model : Tensorflow model
            Trained model for inference.
        LR : numpy array
            3 channel array representation of input image.

        Returns
        -------
        HR : array
            3 channel tensor representing high resolution output image.

        """
        if (len(LR.shape) == 3):
            
            # Adding fourth dimension for batch as model expects 4D tensor
            LR = tf.expand_dim(LR, axis=0)
            
        LR = tf.cast(LR, tf.float32)
        HR = model(LR)
        HR = tf.clip_by_value(HR, 0, 255)
        HR = tf.round(HR)
        HR = tf.cast(HR, tf.uint8)
        return HR
    
    def get_model(self):
        """
        Create a blank copy of the model and add trained weights to the model.

        Returns
        -------
        model : Tensorflow model
            Trained model that can be used for inference.

        """
        model = self.create_model()
        # print(os.getcwd())
        # print(os.listdir())
        model.load_weights('edsr_model.h5')
        return model