# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 14:34:29 2022

@author: Siddharth
"""

import cv2
import os
import tensorflow as tf
import argparse
from constants import Constants
from trainer import Trainer
from utils import get_hr_image, get_hr_video


def parse_args():
    parser = argparse.ArgumentParser(description='Program for super resolution')
    
    # Paths arguments
    parser.add_argument('-i', '--image', help='Test image location', required=False)
    parser.add_argument('-o', '--output_dir', help='Output store location', required=False)
    parser.add_argument('-m', '--model_dir', help='Model location for inference', required=False)
    parser.add_argument('-v', '--video', help='Test video location', required=False)
    
    # Boolean arguments
    parser.add_argument('-t', '--inference', \
                        help='Inference? or Train? Default value: True', \
                        action='store_true')
    parser.add_argument('-g', '--gpu_available', \
                        help="Is GPU available?", action='store_true')
        
    # Numerical arguments
    parser.add_argument('-e', '--epochs',type=int, help='Training epochs', required=False)
    parser.add_argument('-q', '--quant', type=int, help='Quantize model to shrink .pb files', default=0)
    parser.add_argument('-r', '--resBlocks', type=int, help='Number of resBlocks', default=32)
    parser.add_argument('-f', '--filter', type=int, help='Number of filters', default=256)
    parser.add_argument('-s', '--scale', type=int, help='Scaling factor', default=2)
    parser.add_argument('-b', '--batch', type=int, help='Batch size', default=16)
    parser.add_argument('--lr', type=float, help='Learning rate', default=0.0001)
    
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    
    # Manually disable GPU?
    if not args.gpu_available:
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    
    # Check if GPU available, if yes, use GPU
    if tf.test.gpu_device_name():
        print('Using GPU')
    else:
        print("No GPU found")
    
    # Create constant object containing hyper-parameters
    const = Constants(2, None, 200, None, None, None)
    
    SCALE, RGB_MEAN, EPOCHS, LR, SAVE_LOC, MODEL_LOC = const.load_constant()

    # Create trainer object for training and inference
    t = Trainer(os.getcwd(), SCALE, RGB_MEAN, EPOCHS, SAVE_LOC)
    
    # Perform inference for single image
    if args.inference and not args.video:
        
        image = cv2.imread(args.image)
        output_image = get_hr_image(image, t)
        cv2.imwrite('image.png', output_image)
        
        return 
    
    # Perform inference for video
    if args.video:
        print('Loading pre_trained model for inference')
        model = t.get_model()
        get_hr_video(args.video, t, model)              
        
        return
        
    history = t.train_model()

if __name__=='__main__':
    main()