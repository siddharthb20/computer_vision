# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 17:02:54 2022

@author: Siddharth
"""

import os
import tensorflow as tf
from tensorflow.python.data.experimental import AUTOTUNE

class DataLoader():
    def __init__(self, scale=2, subset='train', downgrade='bicubic',
                 images_dir='div2k/images', cache_dir='div2k/cache'):
        
        scales = [2, 3, 4]
        
        if scale in scales:
            self.scale = scale
        else:
            raise ValueError(f'Scale incorrect. Value must be in ${scales}')              
        
        if subset=='train':
            self.image_ids=range(1, 801)
        elif subset=='valid':
            self.image_ids=range(801,901)
        else:
            raise ValueError("Subset must be either 'train' or 'valid'")
        
        self.subset = subset
        self.image_dir = images_dir
        self.cache_dir = cache_dir
        
        os.makedirs(images_dir, exist_ok=True)
        os.makedirs(cache_dir, exist_ok=True)        
        
    
    def __len__(self):
        return len(self.image_ids)
    
    def LR_images_path(self):
        return os.path.join(self.image_dir, f'DIV2K_{self.subset}_LR_bicubic',
                                f'X{self.scale}')
    
    def HR_images_path(self):
        return os.path.join(self.image_dir, f'DIV2K_{self.subset}_HR')
    
    def LR_images_zip(self):
        return f'DIV2K_{self.subset}_LR_bicubic_X{self.scale}.zip'
    
    def HR_images_zip(self):
        return f'DIV2K_{self.subset}_HR.zip'

    def LR_image_name(self, image_id):
        return f'{image_id:04}x{self.scale}.png'
    
    def LR_images(self):
        images_dir = self.LR_images_path()
        return [os.path.join(images_dir, 
                             self.LR_image_name(image_id)) for image_id in self.image_ids]

    def HR_images(self):
        images_dir = self.HR_images_path()
        return [os.path.join(images_dir, 
                             f'{image_id:04}.png') for image_id in self.image_ids]
    
    def LR_cache_files(self):
        return os.path.join(self.cache_dir, 
                            f'DIV2K_{self.subset}_LR_bicubic_X{self.scale}.cache')
    
    def HR_cache_files(self):
        return os.path.join(self.cache_dir, f'DIV2K_{self.subset}_HR.cache')
    
    def LR_cache_index(self):
        return os.path.join(self.cache_dir, 
                            f'DIV2K_{self.subset}_LR_bicubic_{self.scale}.cache')
    
    def HR_cache_index(self):
        return f'{self.HR_cache_files()}.index'
    
    def download_zip(self, file, target_dir, extract=True):
        url = f'http://data.vision.ee.ethz.ch/cvl/DIV2K/{file}'
        target_dir = os.path.abspath(target_dir)
        tf.keras.utils.get_file(file, url, cache_subdir=target_dir, extract=extract)
        os.remove(os.path.join(target_dir, file))
    
    @staticmethod
    def Image_dataset(images):
        dataset = tf.data.Dataset.from_tensor_slices(images)
        dataset = dataset.map(tf.io.read_file)
        dataset = dataset.map(lambda x: tf.image.decode_png(x, channels=3),
                              num_parallel_calls=AUTOTUNE)
        return dataset
    
        
    @staticmethod
    def populate_cache(dataset, cache_file):
        print(f'Caching decoded images in {cache_file}')
        for _ in dataset: pass
        print('Caching completed.')
        
    def LR_dataset(self):
        if not os.path.exists(self.LR_images_path()):
            self.download_zip(self.LR_images_zip(), self.image_dir)
        
        dataset = self.Image_dataset(self.LR_images()).cache(self.LR_cache_files())
        
        if not os.path.exists(self.LR_cache_index()):
            self.populate_cache(dataset, self.LR_cache_files())
        
        return dataset
    
    def HR_dataset(self):
        if not os.path.exists(self.HR_images_path()):
            self.download_zip(self.HR_images_zip(), self.image_dir)
        
        dataset = self.Image_dataset(self.HR_images()).cache(self.HR_cache_files())
        
        if not os.path.exists(self.HR_cache_index()):
            self.populate_cache(dataset, self.HR_cache_files())
            
        return dataset
    
    def augment(self, img_LR, img_HR, hr_crop_size=96):
        
        lr_crop_size = hr_crop_size//self.scale
        lr_img_shape = tf.shape(img_LR)[:2]
        
        lr_width = tf.random.uniform(shape=(), maxval=lr_img_shape[1]-lr_crop_size+1, dtype=tf.int32)
        lr_height = tf.random.uniform(shape=(), maxval=lr_img_shape[0]-lr_crop_size+1, dtype=tf.int32)
        
        hr_width = lr_width*self.scale
        hr_height = lr_height*self.scale
        
        lr_img_cropped = img_LR[lr_height:lr_height + lr_crop_size, lr_width:lr_width + lr_crop_size]
        hr_img_cropped = img_HR[hr_height:hr_height + hr_crop_size, hr_width:hr_width + hr_crop_size]
        
        return lr_img_cropped, hr_img_cropped
    
    
    def compile_dataset(self, batch_size=16, repeat_count=None, random_transform=True):
        dataset = tf.data.Dataset.zip((self.LR_dataset(), self.HR_dataset()))
        
        if random_transform:
            dataset = dataset.map(lambda LR, HR: self.augment(LR, HR))
        
        dataset = dataset.batch(batch_size)
        
        dataset = dataset.repeat(repeat_count)
        
        dataset = dataset.prefetch(buffer_size=AUTOTUNE)
        return dataset
            
    