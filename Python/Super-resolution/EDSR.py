# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 18:10:59 2022

@author: Siddharth
"""

import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Add, Conv2D, Input, Lambda

class EDSR():
    
    def __init__(self,
                 rgb_mean,
                 scale):
        self.scale = scale
        if scale is None:
            self.scale = 2
        if rgb_mean is None:
            rgb_mean = np.array([0.4488, 0.4371, 0.4040])
        self.rgb_mean = rgb_mean * 255
    
    def shuffle_pixels(self, scale):
        return lambda x:tf.nn.depth_to_space(x, scale)
    
    def normalize(self, x):
        return (x-self.rgb_mean)/127.5
    
    def denormalize(self, x):
        return (x+self.rgb_mean)/127.5
    
    def ResBlock(self, x_input, filters, scaling):
        """
        Template for ResBlocks

        Parameters
        ----------
        x_input : Tensor
            Input tensor for current block.
        filters : int
            Number of filters.
        scaling : int
            Number of time the image must be scaled.

        Returns
        -------
        x : Tensor
            Output tensor from the current block.

        """
        x = Conv2D(filters, 3, padding='same', activation='relu')(x_input)
        x = Conv2D(filters, 3, padding='same')(x)
        if scaling:
            x = Lambda(lambda t: t*scaling)(x)
        x = Add()([x_input, x])
        return x
    
    def Upsampling(self, x, filters, scaling):
        """
        Method for the upsampling portion of the EDSR network

        Parameters
        ----------
        x : Tensor
            Input tensor.
        filters : int
            Number of filters.
        scaling : int
            Factor of scaling.

        Returns
        -------
        x : Tensor
            Output tensor after upsampling.

        """
        def upsample(x, factor, **kwargs):
            x = Conv2D(filters * (factor**2), 3, padding='same', **kwargs)(x)
            return Lambda(self.shuffle_pixels(scale=factor))(x)
        
        if scaling == 2:
            x = upsample(x, 2, name='conv2d_1_scale_2')
        elif scaling == 3:
            x = upsample(x, 3, name='conv2d_1_scale_3')
        elif scaling == 4:
            x = upsample(x, 2, name='conv2d_1_scale_1')
            x = upsample(x, 2, name='conv2d_1_scale_2')
        
        return x
    
    def super_scaler(self, filters=64, res_blocks=8, res_block_scaling=None):
        """
        Method to compile all the residual blocks and output the final model.

        Parameters
        ----------
        filters : int, optional
            Number of filters. The default is 64.
        res_blocks : int, optional
            Number of residual blocks. The default is 8.
        res_block_scaling : int, optional
            Scaling from the residual block. The default is None.

        Returns
        -------
        Model : Tensorflow model
            Compiled EDSR model.

        """
        x_input = Input(shape=(None, None, 3))
        x = self.normalize(x_input)
        
        x = x_res_block = Conv2D(filters, 3, padding='same')(x)
        
        for i in range(res_blocks):
            x_res_block = self.ResBlock(x_res_block, filters, res_block_scaling)
        
        x_res_block = Conv2D(filters, 3, padding='same')(x_res_block)
        
        x = Add()([x, x_res_block])
        
        x = self.Upsampling(x, filters, self.scale)
        x = Conv2D(3, 3, padding='same')(x)
        
        x = self.denormalize(x)
        
        return Model(x_input, x, name='EDSR')
        

        