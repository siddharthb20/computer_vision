# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 23:27:22 2022

@author: Siddharth
"""
import numpy as np
import os
from tensorflow.keras.optimizers.schedules import PiecewiseConstantDecay

class Constants():
    def __init__(self, scale, rgb, epochs, save_loc, learning_rate,
                 model_loc):
        self.scale = scale
        self.rbg = rgb
        self.epochs = epochs
        self.save_loc = save_loc
        self.lr = learning_rate
        self.model = model_loc
        
    def load_constant(self):
        self.scale = 2
        self.rgb = np.array([0.4488, 0.4371, 0.4040])
        if self.epochs == None:
            self.epochs = 200
        if self.lr == None:
            self.lr = PiecewiseConstantDecay(boundaries=[20000], values=[1e-4, 5e-5])
        if self.save_loc == None:
            self.save_loc = os.getcwd()
        if self.model == None:
            self.model = os.getcwd()
        return self.scale, self.rgb, self.epochs, self.lr, self.save_loc, self.model