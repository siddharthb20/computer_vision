# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 23:59:53 2022

@author: Siddharth
"""

import os
import cv2
import tensorflow as tf
from tqdm import tqdm
import imageio
import moviepy.editor as mp

def get_filename(video_loc):
    """
    Function to extract filename and create output folder for videos

    Parameters
    ----------
    video_loc : str
        Path to video.

    Returns
    -------
    file_name : str
        Name of video.

    """
    folder_name, file_name = os.path.split(video_loc)
    filename = os.path.splitext(file_name)[0]
    path = os.path.join(folder_name, filename)
    if not os.path.isdir(path):
        os.mkdir(path)
    os.chdir(path)
    return file_name

def get_hr_image(image, obj, model=None):
    """
    Function to run inference on image

    Parameters
    ----------
    image : numpy array
        Image to perform SR on.
    obj : class object
        Object of class EDSR.
    model : keras model
        Trained model to be used for image super resolution.

    Returns
    -------
    output_image : numpy array
        HR image.

    """
    print('Loading pre-trained model for inference')
    if model == None:
        model = obj.get_model()
    img = tf.expand_dims(image, axis=0)
    hr_image = obj.inference_image(model, img)
    output_image = hr_image[0].numpy()
    return output_image


def get_hr_video(video_loc, obj, model=None):
    """
    Function to run inference on videos
    
    Parameters
    ----------
    video_loc : str
        Location of video.
    obj : class object
        Object of class EDSR.
    model : keras model
        Trained model to be used for image super resolution.
    
    """

    if model == None:
        model = obj.get_model()
    filename = get_filename(video_loc)
    path = os.getcwd()
    video = cv2.VideoCapture(video_loc)
    success, image = video.read()
    length = int(video.get(cv2.CAP_PROP_FRAME_COUNT)) 
    height, width, _ = image.shape
    high_video = []
    for i in tqdm(range(length)):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        img = tf.expand_dims(image, axis=0)
        hr_image = obj.inference_image(model, img)
        frame = hr_image[0].numpy()
        high_video.append(frame)
        if not ((i+1) % 2000): 
            imageio.mimsave(os.path.join(path, f'{filename}_fragment{i+1}_HD.mp4'), high_video, fps=30)
            high_video = []
        success, image = video.read()

    if len(high_video)>0:
        imageio.mimsave(os.path.join(path, f'{filename}_fragment{length}_HD.mp4'), high_video, fps=30)
    final_clip = merge_clips(filename, add_audio=True, audio=video_loc)
    compile_audio(video_loc, final_clip)
    file_list = os.listdir()
    
    # Cleanup files
    for f in file_list:
        if 'fragment' in f or '.mp3' in f:
            os.remove(os.path.join(path, f))


def merge_clips(final_filename, audio=None, add_audio=False):
    """
    Merge all the clips in the current folder

    Parameters
    ----------
    final_filename : str
        Name of the file to be generated.

    Returns
    -------
    final_clip_loc : str
        Location where the merged clip is stored.

    """
    videos = [v for v in os.listdir() if '.mp4' in v]
    videos = sorted(videos)
    clip_list = []
    for file in tqdm(videos):
        filePath = os.path.join(os.getcwd(), file)
        video = mp.VideoFileClip(filePath)
        clip_list.append(video)

    # final_clip.to_videofile(f'{final_filename}_HD.mp4', fps=30, remove_temp=True)   
    final_clip = mp.concatenate_videoclips(clip_list)
    if audio==None and add_audio:
        raise ValueError("No audio passed for audio compilation.")
    if add_audio:
        audio = get_audio(audio)
        final_clip = compile_audio(final_clip, audio)
    final_clip.to_videofile(f'{final_filename}_HD.mp4', fps=30)
    final_clip_loc = os.path.join(os.getcwd(), f'{final_filename}_HD.mp4')
    return final_clip_loc
    
def get_audio(audio):
    """
    Function to extract audio from original video files

    Parameters
    ----------
    audio : str
        Link to the original audio/video files.

    Returns
    -------
    clip : audio oject
        MoviePy audio object.

    """
    if '.mp4' in audio:
        clip = mp.VideoFileClip(audio)
        # clip.audio.write_audiofile('result.mp3')
        return clip.audio
    else:
        clip = mp.AudioFileClip(audio)
        return clip
    


def compile_audio(video, audio=None):
    """
    Adding audio to high resolution video

    Parameters
    ----------
    video : str
        Video object.
    audio : str
        Audio object.

    Returns
    -------
    final_video : video obj
        Compiled video object

    """
    new_audio_clip = mp.CompositeAudioClip([audio])
    final_video = video.set_audio(new_audio_clip)
    return final_video
