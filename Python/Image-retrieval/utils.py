import os
import scipy.io
import tarfile
import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from keras.layers import Lambda

def get_testdata_from_mat(mat_dir):

    data = scipy.io.loadmat(mat_dir)
    labels, names = [], []
    labels = [int(i[4]) for i in data['annotations'][0]]
    names = [str(i[5][0]) for i in data['annotations'][0]]
    return labels, names

def get_traindata_from_mat(mat_dir):

    print(mat_dir)
    # directory = os.path.split(mat_dir)[0]
    # files = os.listdir(directory)
    # if not 'devkit' in files:
    #     file = tarfile.open(mat_dir)
    #     file.extractall(directory)
    #     file.close()
    
    # label_dir = os.path.join(directory, 'devkit')
    labels_file = mat_dir
    data = open(labels_file, 'r')
    labels = data.read()
    labels = labels.split('\n')
    labels.pop()
    labels = [int(i) for i in labels]
    data.close()

    return labels

def to_tensor(img):
    img = np.expand_dims(img, axis=0)
    img_tensor = tf.cast(img, tf.float32)
    return img_tensor

def triplet_loss(tensor_triplet):
    x, y, z = tensor_triplet
    pos_diff = K.square(x-y)
    neg_diff = K.square(x-z)
    combined_value = pos_diff-neg_diff
    return combined_value