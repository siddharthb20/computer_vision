import tensorflow as tf
from tensorflow.keras import applications
from tensorflow.keras.layers import Input, Conv2D, Subtract, Flatten
from tensorflow.keras.layers import Dense, UpSampling2D
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Lambda
from constants import IMG_SIZE, OPTIM, EPOCHS, BATCH_SIZE
import numpy as np
from tqdm import tqdm
from utils import triplet_loss, to_tensor
import os
import tarfile
import faiss

class Models():
    def __init__(self, data, model_path):
        self.model_path = model_path
        self.data = data
    
    def base_model(self):
        input_img = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        pretrained_VGG = applications.vgg16.VGG16(weights='imagenet', \
            include_top=False, input_shape=(IMG_SIZE, IMG_SIZE, 3))
        VGG_model = Model(pretrained_VGG.input, pretrained_VGG.layers[-1].output)

        x = VGG_model(input_img)
        x = Flatten()(x)
        x = Dense(4096, activation='sigmoid')(x)

        final_model = Model(input_img, x)
        # print(f'Final model type:{final_model}')
        # final_model.summary()
        return final_model

    def combined_model(self):
        anchor_input = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        pos_input = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        neg_input = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        anchor_model = self.base_model()(anchor_input) 
        anchor = anchor_model(anchor_input)
        pos_model = self.base_model(tied_to=anchor_model)(pos_input)
        neg_model = self.base_model(tied_to=anchor_model)(neg_input)
        # output = Subtract()([upper_model, lower_model])
        output = Lambda(triplet_loss)([anchor, pos_model, neg_model])
        output = Dense(1, activation='sigmoid')(output)
        output_model = Model([anchor_input, pos_input, neg_input], output)
        self.model = output_model
        return output_model

    def compile_model(self):
        print("Compiling model")
        self.model = self.combined_model()
        self.model.summary()
        optimizer = OPTIM
        self.model.compile(loss='binary_crossentropy', optimizer=optimizer)

    def dev_test(self):
        self.compile_model()
        anchor, pos, neg = self.data.generate_batch(self.data.train_names, self.data.train_labels)
        anchor, pos, neg = anchor[0], pos[0], neg[0]
        print(f'Anchor shape: {anchor.shape}    Pos shape: {pos.shape}  Neg shape: {neg.shape}')
        print(type(self.model))
        output = self.model.predict([anchor, pos, neg])
        print(type(output))
        print(output)
        # print(output.shape)
        print(output.shape)


    def train(self):
        self.compile_model()
        data_size = len(self.data.train_names)//10
        for epoch in range(EPOCHS):
            for b in tqdm(range(data_size//BATCH_SIZE)):
                anchor, pos, neg = self.data.generate_batch(self.data.train_names, self.data.train_labels)
                anchor, pos, neg = np.array(anchor), np.array(pos), np.array(neg)
                label = np.zeros((1, 1))
                loss = self.model.fit([anchor[0], pos[0], neg[0]], label)
            print(f'Epoch: {epoch+1} completed')
            if loss.history['loss'][0] < 1e-30:
                self.save_model()
                break

    
    def save_model(self, compress=True):

        self.model.save(os.getcwd())
        if compress:
            model_files = [i for i in os.listdir() if self.model_file_check(i)]
            with tarfile.open('model.tar.gz', 'w:gz') as tar:
                for file in model_files:
                    tar.add(os.path.join(os.getcwd(), file), arcname=file)


    def model_file_check(self, object_name):
        complete_path = os.path.join(os.getcwd(), object_name)
        if os.path.isdir(complete_path):
            if object_name == 'assets' or object_name == 'variables':
                return True
        
        if os.path.isfile(complete_path):
            extension = os.path.splitext(object_name)[1]
            if 'pb' in extension:
                return True


    
    def loadmodel(self):
        if not self.model_path:
            raise ValueError('No model found. Try again with a different path.')
        
        if 'gz' in os.path.splitext(self.model_path)[1]:
            file = tarfile.open(self.model_path)
            file.extractall(os.getcwd())
            file.close()
        
        self.model = load_model(os.getcwd())

    
    def extract_conv(self):
        self.loadmodel()
        feature_model = self.model.layers[4]
        input_img = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        features = feature_model(input_img)
        model = Model(input_img, features)
        return model
        
    def store_image_representation(self):
        self.im_indices = []
        self.faiss_index = faiss.IndexFlatL2(4096)
        model = self.extract_conv()

        for f in tqdm(self.data.train_names):
            img_path = os.path.join(self.data.train_image_dir, f)
            img = self.data.read_image(img_path)
            img = to_tensor(img)
            pred = model.predict(img, verbose=0)
            self.faiss_index.add(pred)
            self.im_indices.append(f)
            

    def retrieve(self, test_image):
        test_image = self.data.read_image(test_image)
        test_image = to_tensor(test_image)
        model = self.extract_conv()
        pred = model.predict(test_image)
        _, I = self.faiss_index.search(pred, 5)
        # print(f"Best match image: {self.im_indices[I[0][0]]}")
        return self.im_indices[I[0][0]]


    def inference(self, test_img):
        
        # model = self.extract_conv()
        self.store_image_representation()
        self.retrieve(test_img)

    def compute_accuracy(self, test_data_dir):

        test_data_files = sorted(os.listdir(test_data_dir))
        self.store_image_representation()
        test_labels = self.data.test_labels
        matches = []
        for idx, f in tqdm(enumerate(test_data_files)):
            test_img = os.path.join(test_data_dir, f)
            best_match = self.retrieve(test_img)
            best_match_path = os.path.join(self.data.train_image_dir, best_match)
            ind = self.data.train_names.index(best_match)
            train_label = self.data.train_labels[ind]
            match = 1 if train_label == test_labels[idx] else 0
            matches.append(match)
        print(f'Accuracy: {sum(matches)/len(matches)}')
        