import os
import constants
from utils import get_testdata_from_mat, get_traindata_from_mat, to_tensor
import cv2
import numpy as np
import random

class Data():

    def __init__(self, image_dir, test_dir=None):
        self.train_image_dir = os.path.join(image_dir, 'cars_train')
        if not self.check_images(self.train_image_dir):
            raise Exception('No training data found. Check input directory.')
        else:
            print(f'Found {self.check_images(self.train_image_dir)} training images')
        self.label_dir = image_dir
        self.extract_labels()
        

    def check_images(self, image_dir):
        images = sorted(os.listdir(image_dir))
        extensions = [os.path.splitext(i)[1] for i in images \
            if os.path.splitext(i)[1] in constants.IMG_FORMATS]

        if len(extensions) < 1:
            return False
        return len(images)+1
    
    def extract_labels(self):
        self.label_dir = os.path.join(self.label_dir, 'devkit')
        files = sorted(os.listdir(self.label_dir))
        names = [os.path.splitext(i)[0] for i in files]
        ext = [os.path.splitext(i)[1] for i in files]
        for n, e in zip(names, ext):

            if 'test' in n and 'mat' in e:
                test_label = n+e
            elif 'train_perfect_preds' in n:
                train_annot = n+e

        self.test_labels, self.test_names = get_testdata_from_mat(os.path.join(self.label_dir, test_label))
        self.train_labels = get_traindata_from_mat(os.path.join(self.label_dir, train_annot))
        self.train_names = sorted(os.listdir(self.train_image_dir))


    def read_image(self, img_path, show_image=False):

        if type(img_path) is list:
            img_path = img_path[0]
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # img = self.resize_image(img)
        im = cv2.resize(img, (224, 224))
        img = im / 255.0
        
        if show_image:
            cv2.imshow('img', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return img

    
    def resize_image(self, img):
        h, w, _ = img.shape
        if h > w:
            delta = h - w
            img = img[int(delta/2):, :, :]
            img = img[:-int(delta/2), :, :]
        
        if w > h:
            delta = w - h
            img = img[:, int(delta/2):, :]
            img = img[:, :-int(delta/2), :]
        return img


    def generate_batch(self, images, labels):
        index = [i for i in range(len(images))]
        indices = random.sample(index, constants.BATCH_SIZE)
        chosen_indexes = [self.train_names[i] for i in indices]
        chosen_labels = [self.train_labels[i] for i in indices]
        anchor_images = [self.read_image(os.path.join(self.train_image_dir, i)) for i in chosen_indexes]
        pos_idx = [self.get_matching_labels(i, labels) for i in chosen_labels]

        positives = [[os.path.join(self.train_image_dir, images[i]) for i in pos_idx[j]] for j in range(len(pos_idx))]
        # print(pos_idx)
        neg_idx = []
        for i in range(len(pos_idx)):
            negs = list(set(labels)-set(pos_idx[i]))
            neg_idx.append(negs)
        # print(neg_idx)
        negatives = [[os.path.join(self.train_image_dir, images[i]) for i in neg_idx[j]] for j in range(len(neg_idx))]

        anchor_tensor = [to_tensor(i) for i in anchor_images]

        # print(len(anchor_images), positives, negatives)
        pos_tensor = [to_tensor(self.read_image(random.sample(i, 1))) for i in positives]
        neg_tensor = [to_tensor(self.read_image(random.sample(i, 1))) for i in negatives]

        return [anchor_tensor, pos_tensor, neg_tensor]          

        # print(temp)
        # for img in images:
            
        # print(img)


    def get_matching_labels(self, label, labels):
        matching_indices = [i for i in range(len(labels)) if labels[i]==label]
        return matching_indices