import os
import argparse
from dataloader import Data
from model import Models

def parse_args():
    parser = argparse.ArgumentParser(description='Image retrieving program')
    parser.add_argument('-i', '--input_dir', help='Location of the input images')
    parser.add_argument('-m', '--model_path', help='Path to the trained model')
    parser.add_argument('-t', '--train', help='Perform training?', action='store_true')
    parser.add_argument('-f', '--find', help='Search image')
    parser.add_argument('-a', '--accuracy', help='Test data directory')

    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    data = Data(args.input_dir)
    # data.generate_batch(data.train_names, data.train_labels)
    # image_path = os.path.join(data.train_image_dir, data.train_names[122])
    model_branch = Models(data, args.model_path)
    # model_branch.dev_test()
    # model_branch.train()
    if args.train:
        model_branch.train()
    # elif not args.find:
        # model_branch.store_image_representation()
    elif args.accuracy:
        model_branch.compute_accuracy(args.accuracy)
    # print(args.train)
    

if __name__ == "__main__":
    main ()
