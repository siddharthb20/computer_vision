from tensorflow.keras.optimizers import SGD, Adam

IMG_FORMATS = ['.jpg', '.jpeg', '.png']
IMG_SIZE = 224
BATCH_SIZE = 16
LEARNING_RATE = 0.005
OPTIM = Adam(learning_rate=LEARNING_RATE)
EPOCHS = 10